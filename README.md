----------------------------------
| PLEASE READ BEFORE CONTINUING: |
----------------------------------

* This git repository is part of the research paper "Internet: from a graduation-perspective".
* This git repository is publicly available for reproducability purposes.
* This git repoistory is made by Quincy Muskita (student number: s3066142).
* Please contact me if there are any issues.

---------------
| HOW TO USE: |
---------------

* Make sure you run the commands on a Linux OS in the kernel.

1. Clone the git using the command "git clone https://quincymaxwellmuskita@bitbucket.org/quincymaxwellmuskita/iwo.git" in a directory of choice.
2. Move into the downloaded git directory
3. Make the shell script executable using the command "chmod +x script.sh"
4. Run the shell script using the command "./script.sh"
5. The script should now print a list of countries with data available for research and two random countries and their corresponding place (number) in the aforementioned list.
6. Go to the following URLs to access the datasets through the EU Open Data Portal:

    * https://data.europa.eu/euodp/en/data/dataset/5cb7d6Dty2PMloa8B8yNfg
    * https://data.europa.eu/euodp/en/data/dataset/RQWIrh9ka7jr87ndDtqN4g

7. Edit the datasets through the Eurostat website as described in the paper.
8. Run a chi-squared test via the following URL or one of your own choice.

    * http://turner.faculty.swau.edu/mathematics/math241/materials/contablecalc/
