#!/usr/bin/python3

# File name: random2.py
# This program produces 2 random integers in the range 1-23
# Author: Q.M. Muskita
# Date: 14-3-2018

from random import randint


def main():
    country_list = ["Belgium", "Bulgaria", "Czech", "Denmark", "Germany", "Estonia",
                    "Ireland", "Cyprus", "Latvia", "Lithuania", "Hungary",
                    "Netherlands", "Malta", "Austria", "Poland", "Portugal", 
                    "Romania", "Slovenia", "Finland", "Sweden", "UK", "Iceland",
                    "Norway"]
    print("Here are your two random integers and corresponding countries:")
    for i in range(2):
        x = randint(0,22)
        print("{0}: {1}".format(x+1, country_list[x]))

if __name__ == "__main__":
    main()
